--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comments; Type: TABLE; Schema: public; Owner: freeman; Tablespace: 
--

CREATE TABLE comments (
    id character varying(255) NOT NULL,
    comment character varying(255),
    email character varying(255),
    name character varying(255),
    post_id character varying(255)
);


ALTER TABLE public.comments OWNER TO freeman;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: freeman
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO freeman;

--
-- Name: posts; Type: TABLE; Schema: public; Owner: freeman; Tablespace: 
--

CREATE TABLE posts (
    id character varying(255) NOT NULL,
    date timestamp without time zone,
    post character varying(255),
    title character varying(255),
    author_id character varying(255)
);


ALTER TABLE public.posts OWNER TO freeman;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: freeman; Tablespace: 
--

CREATE TABLE tags (
    id character varying(255) NOT NULL,
    name character varying(255),
    post_id character varying(255)
);


ALTER TABLE public.tags OWNER TO freeman;

--
-- Name: users; Type: TABLE; Schema: public; Owner: freeman; Tablespace: 
--

CREATE TABLE users (
    id character varying(255) NOT NULL,
    password character varying(255),
    username character varying(255)
);


ALTER TABLE public.users OWNER TO freeman;

--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: freeman
--

COPY comments (id, comment, email, name, post_id) FROM stdin;
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: freeman
--

SELECT pg_catalog.setval('hibernate_sequence', 1, true);


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: freeman
--

COPY posts (id, date, post, title, author_id) FROM stdin;
00673237-1fd6-4b3f-9065-e45ab80394a6	2014-12-21 22:57:01.173	This is my first post	First post ever	43964742-da7e-41e8-985e-e6fc86cb3c8d
6dac448c-271b-4d92-b4a4-ca0ffd8d4e56	2014-12-21 23:02:46.584	With this post I'm going to conclude today's programmin session and have a supper)	One more post for today	43964742-da7e-41e8-985e-e6fc86cb3c8d
b231e2ba-cf0b-417f-a764-567760ef50c3	2014-12-21 23:04:59.368	Eventually we succeeded and this tutorial should give Andrew a possibility to study enterprise programming and find great job!\r\n	Cooool post	43964742-da7e-41e8-985e-e6fc86cb3c8d
\.


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: freeman
--

COPY tags (id, name, post_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: freeman
--

COPY users (id, password, username) FROM stdin;
43964742-da7e-41e8-985e-e6fc86cb3c8d	qwest123	freeman
\.


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: freeman; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: posts_pkey; Type: CONSTRAINT; Schema: public; Owner: freeman; Tablespace: 
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: freeman; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: freeman; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: fk_2ocgo3lfadb3wq0tx8wyt7sj2; Type: FK CONSTRAINT; Schema: public; Owner: freeman
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_2ocgo3lfadb3wq0tx8wyt7sj2 FOREIGN KEY (post_id) REFERENCES posts(id);


--
-- Name: fk_7oqt3elt7nlmr87tin12l2vv8; Type: FK CONSTRAINT; Schema: public; Owner: freeman
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT fk_7oqt3elt7nlmr87tin12l2vv8 FOREIGN KEY (author_id) REFERENCES users(id);


--
-- Name: fk_jynb55aehxvu3m14q0spmxr0c; Type: FK CONSTRAINT; Schema: public; Owner: freeman
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT fk_jynb55aehxvu3m14q0spmxr0c FOREIGN KEY (post_id) REFERENCES posts(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


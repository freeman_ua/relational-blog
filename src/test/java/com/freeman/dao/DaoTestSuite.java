package com.freeman.dao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The purpose of this class is to run all dao tests at once.
 * 
 * @author freeman
 *
 */
@RunWith(Suite.class)
@SuiteClasses({PostDaoTest.class, CommentDaoTest.class, TagDaoTest.class, UserDaoTest.class})
public class DaoTestSuite {

}

package com.freeman.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.freeman.domain.Comment;
import com.freeman.domain.Post;
import com.freeman.domain.User;
import com.freeman.util.TestHibernateUtil;

/**
 *  Unit test for <code>PostDao</code> class.
 * 
 * @author freeman
 *
 */
public class PostDaoTest {
    private static SessionFactory factory = TestHibernateUtil.getSessionFactory();

    private PostDao testee;

    @Before
    public void setUp() {
        testee = new PostDao(factory);
    }

    private Post createTestPost() {
        User author = new User("Dick Channie", "secret");
        Comment firstComment = new Comment("First comment", "Test post comment", "test@mail.com");
        Comment secondComment = new Comment("Second comment", "Second post comment", "test@mail.com");
        Comment thirdComment = new Comment("Third comment", "Third post comment", "test@mail.com");
        Post post = new Post("First post ever", "There was a time...", author);
        post.addComment(firstComment);
        post.addComment(secondComment);
        post.addComment(thirdComment);
        return post;
    }

    @Test
    public void itShouldCreatePost() {
        Post post = createTestPost();
        String id = testee.createPost(post);

        Assert.assertNotNull(id);

        Session session = factory.getCurrentSession();
        session.beginTransaction();
        Post found = (Post) session.load(Post.class, id);
        session.getTransaction().commit();

        Assert.assertNotNull(found);
    }

    @Test
    public void itShouldGetPost() {
        Post post = createTestPost();
        String id = testee.createPost(post);

        Assert.assertNotNull(id);

        Post found = testee.getPost(id);

        Assert.assertNotNull(found);
        Assert.assertEquals(post.getId(), found.getId());
    }

    @Test
    public void itShouldGetPosts() {
        Post post = createTestPost();
        testee.createPost(post);

        List<Post> posts = testee.getPosts();

        Assert.assertFalse(posts.isEmpty());
//        Assert.assertTrue(posts.contains(post));
    }

    @Test
    public void itShouldUpdatePost() {
        Post post = createTestPost();
        String id = testee.createPost(post);

        Assert.assertNotNull(post);

        Post found = testee.getPost(id);

        Assert.assertEquals("First post ever", found.getTitle());

        found.setTitle("The time of a five realms!");

        testee.update(found);
        found = testee.getPost(id);

        Assert.assertEquals("The time of a five realms!", found.getTitle());
    }
}

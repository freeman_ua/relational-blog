package com.freeman.dao;

import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.freeman.domain.User;
import com.freeman.util.TestHibernateUtil;

/* Put appropriate java documentation comment here, please. */
public class UserDaoTest {
    private static SessionFactory factory = TestHibernateUtil.getSessionFactory();

    private UserDao testee;

    @Before
    public void setUp() {
        testee = new UserDao(factory);
    }

    private User createTestUser() {
        return new User("test-user", "secret");
    }

    @Test
    public void itShouldCreateUser() {
        Assert.assertTrue(true);
    }

    @Test
    public void itShouldFindUserByName() {
        User user = createTestUser();
        String id = testee.createUser(user);

        Assert.assertNotNull(id);

        User found = testee.getUserByName(user.getUsername());

        Assert.assertNotNull(found);
        Assert.assertEquals(user.getUsername(), found.getUsername());
    }
}

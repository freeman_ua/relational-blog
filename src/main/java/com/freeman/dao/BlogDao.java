package com.freeman.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.ResultTransformer;

import com.freeman.util.HibernateUtil;

/**
 * Base entity for blog dao instances.
 * 
 * @author freeman
 *
 */
public abstract class BlogDao {
    protected static SessionFactory factory = HibernateUtil.getSessionFactory();

    /**
     *  Constructor that is used for injecting of a different hibernate's session factory
     * instances to separate test and production databases.
     * 
     * @param factory - session factory reference
     */
    public BlogDao(SessionFactory factory) {
        BlogDao.factory = factory;
    }

    /**
     *  General implementation of the accessor method.
     * 
     * @param clazz - entity needs to be loaded
     * @param id - entity identifier
     * @return - found entity
     */
    protected Object get(Class<?> clazz, String id) {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        Object entity = session.get(clazz, id);
        session.getTransaction().commit();
        return entity;
    }

    /**
     *  General implementation of the update method.
     * 
     * @param entity - object to be updated
     */
    protected void update(Object entity) {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
    }

    /**
     *  General implementation of the persist method.
     * 
     * @param entity - object to be saved
     * @return - generated identifier
     */
    protected String save(Object entity) {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        String id = (String) session.save(entity);
        session.getTransaction().commit();
        return id;
    }

    protected void delete() {
        /* Feel free to implement this ;-) */
    }

    protected List<?> list(Class<?> clazz) {
        List<?> entities = null;
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        entities = session.createCriteria(clazz).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        session.getTransaction().commit();
        return entities;
    }
}

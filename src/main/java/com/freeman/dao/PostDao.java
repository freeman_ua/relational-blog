package com.freeman.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.freeman.domain.Post;

/**
 * Data access object for Post entity.
 * 
 * @author freeman
 *
 */
public class PostDao extends BlogDao {
    public PostDao(SessionFactory factory) {
        super(factory);
    }

    public String createPost(Post post) {
        return save(post);
    }

    public Post getPost(String id) {
        return (Post) get(Post.class, id);
    }

    public List<Post> getPosts() {
        return (List<Post>) list(Post.class);
    }

    public void updatePost(Post post) {
        update(post);
    }
}

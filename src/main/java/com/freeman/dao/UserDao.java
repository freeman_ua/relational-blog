package com.freeman.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.freeman.domain.User;

/**
 * Data access object for User entity.
 * 
 * @author freeman
 *
 */
public class UserDao extends BlogDao {
    public UserDao(SessionFactory factory) {
        super(factory);
    }

    public String createUser(User user) {
        return save(user);
    }

    public User getUser(String id) {
        return (User) get(User.class, id);
    }

    public void updateUser(User user) {
        update(user);
    }

    public User getUserByName(String username) {
        Session session = factory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("from User where username like :username");
        query.setParameter("username", username);
        User user = (User) query.uniqueResult(); 
        session.getTransaction().commit();
        return user;
    }
}

package com.freeman.dao;

import org.hibernate.SessionFactory;

import com.freeman.domain.Tag;

/**
 * Data access object for Tag entity.
 * 
 * @author freeman
 *
 */
public class TagDao extends BlogDao {
    public TagDao(SessionFactory factory) {
        super(factory);
    }

    public String createTag(Tag tag) {
        return save(tag);
    }

    public Tag getTag(String id) {
        Tag tag = (Tag) get(Tag.class, id);
        return tag;
    }

    public void updateTag(Tag tag) {
        update(tag);
    }
}

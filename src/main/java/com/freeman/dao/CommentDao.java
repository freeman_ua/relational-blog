package com.freeman.dao;

import org.hibernate.SessionFactory;

import com.freeman.domain.Comment;

/**
 * Data access object for Comment entity.
 * 
 * @author freeman
 *
 */
public class CommentDao extends BlogDao {
    public CommentDao(SessionFactory factory) {
        super(factory);
    }

    public String createComment(Comment comment) {
        return save(comment);
    }

    public Comment getComment(String id) {
        Comment comment = (Comment) get(Comment.class, id);
        return comment;
    }

    public void updateComment(Comment comment) {
        update(comment);
    }
}

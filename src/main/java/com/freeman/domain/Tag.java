package com.freeman.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.freeman.util.SecurityUtils;

/**
 * Tag entity.
 * 
 * @author freeman
 *
 */
@Entity
@Table(name = "TAGS")
public class Tag {
    @Id
//    @GeneratedValue
//    private Long id;
    private String id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    private Tag() {
        this.id = SecurityUtils.generateId();
    }

    public Tag(String name, Post post) {
        this.id = SecurityUtils.generateId();
        this.name = name;
        this.post = post;
    }

    private void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    private void setPost(Post post) {
        this.post = post;
    }

    public Post getPost() {
        return this.post;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

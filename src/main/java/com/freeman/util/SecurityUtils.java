package com.freeman.util;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SecurityUtils {
    public static Map<Long, String> users = new HashMap<Long, String>();

    public static void addUser(Long userId, String username) {
        users.put(userId, username);
    }

    public static void removeUser(Long userId) {
        users.remove(userId);
    }

    public static String generateId() {
        return UUID.randomUUID().toString();
    }
}

package com.freeman.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freeman.dao.PostDao;
import com.freeman.dao.UserDao;
import com.freeman.domain.Comment;
import com.freeman.domain.Post;
import com.freeman.util.HibernateUtil;

/**
 * Java servlet class that manages posts. 
 * 
 * @author freeman
 *
 */
public class PostManagerServlet extends HttpServlet {
    private PostDao postDao;
    private UserDao userDao;

    public PostManagerServlet() {
        postDao = new PostDao(HibernateUtil.getSessionFactory());
        userDao = new UserDao(HibernateUtil.getSessionFactory());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String postCreation = request.getParameter("create");
        PrintWriter writer = response.getWriter();

        String postId = extractPostId(request);
        if (postId == null && postCreation == null) {
            listAllPosts(writer);
        } else if (postCreation != null && Boolean.parseBoolean(postCreation)) {
            getPostCreationForm(writer);
        } else {
            findPostById(writer, postId);
        }
    }

    private void findPostById(PrintWriter writer, String postId) {
        Post post = postDao.getPost(postId);

        writer.println("<html>");
        writer.println("  <head>");
        writer.println("    <link href=\""+getServletContext().getContextPath()+"/css/main.css\" rel=\"stylesheet\" type=\"text/css\">");
        writer.println("  </head>");
        writer.println("  <body>");
        writer.println("    <h2 class=\"center\">"+post.getTitle()+"</h2>");
        writer.println("    <form action=\"post\" method=\"post\" class=\"blogForm\">");
        writer.println("      <p class=\"narrative\">");
        writer.println(post.getPost());
        writer.println("      </p>");
        writer.println("      <h3 class=\"center comment\">Comments</h2>");
        for (Comment comment: post.getComments()) {
            writer.println("      <blockquote>");
            writer.println("        <p class=\"narrative\">");
            writer.println(comment.getComment());
            writer.println("        </p>");
            writer.println("      </blockquote>");
        }
        writer.println("          <p>");
        writer.println("            <label for=\"commet\" class=\"label\">Comment: </label>");
        writer.println("            <textarea name=\"comment\" id=\"comment\" rows=\"5\" cols=\"35\"></textarea>");
        writer.println("          </p>");
        writer.println("          <p>");
        writer.println("            <input type=\"submit\" value=\"Leave comment\">");
        writer.println("          </p>");
        writer.println("    </form>");
        writer.println("  </body>");
        writer.println("</html>");
    }

    private void getPostCreationForm(PrintWriter writer) {
        writer.println("<html>");
        writer.println("  <head>");
        writer.println("    <link href=\""+getServletContext().getContextPath()+"/css/main.css\" rel=\"stylesheet\" type=\"text/css\">");
        writer.println("  </head>");
        writer.println("  <body>");
        writer.println("    <h2 class=\"center\">Create new post</h2>");
        writer.println("    <form action=\"post\" method=\"post\" class=\"blogForm\">");
        writer.println("      <p>");
        writer.println("        <label for=\"title\" class=\"label\">Title: </label>");
        writer.println("        <input type=\"text\" name=\"title\" id=\"title\">");
        writer.println("      </p>");
        writer.println("      <p>");
        writer.println("        <label for=\"post\" class=\"label\">Post: </label>");
        writer.println("        <textarea name=\"post\" id=\"post\" rows=\"5\" cols=\"35\"></textarea>");
        writer.println("      </p>");
        writer.println("      <p>");
        writer.println("        <input type=\"submit\" value=\"Create Thread\">");
        writer.println("      </p>");
        writer.println("    </form>");
        writer.println("  </body>");
        writer.println("</html>");
    }

    private void listAllPosts(PrintWriter writer) {
        List<Post> posts = postDao.getPosts();

        String contextPath = getServletContext().getContextPath();

        writer.println("<html>");
        writer.println("  <head>");
        writer.println("    <link href=\""+contextPath+"/css/main.css\" rel=\"stylesheet\" type=\"text/css\">");
        writer.println("    <link href=\""+contextPath+"/css/post-list.css\" rel=\"stylesheet\" type=\"text/css\">");
        writer.println("  </head>");
        writer.println("  <body>");
        writer.println("    <h2>Post Listing</h2>");
        writer.println("    <div>");
        writer.println("        <ul class=\"post-list\">");
        for (Post post: posts) {
            writer.println("        <li><a href=\"post?id="+post.getId()+"\">"+post.getTitle()+"</a></li>");
        }
        writer.println("        </ul>");
        writer.println("    </div>");
        writer.println("     <p>");
        writer.println("       <a class=\"buttonLink\" href=\"./post?create=true\">Create Post</a>");
        writer.println("     </p>");
        writer.println("  </body>");
        writer.println("</html>");
    }

    private String extractPostId(HttpServletRequest request) {
        String postId = request.getParameter("id");
        return postId != null ? postId : null;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String author = (String) request.getSession().getAttribute("userId");
        String title = (String) request.getParameter("title");
        String postBody = (String) request.getParameter("post");

        Post newPost = new Post(title, postBody, userDao.getUser(author));
        postDao.createPost(newPost);

//        redirect(request, response, "/post");
        response.sendRedirect("./post");
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response, String url) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPut(request, response);
    }
}

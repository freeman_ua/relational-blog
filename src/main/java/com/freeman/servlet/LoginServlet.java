package com.freeman.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.freeman.dao.UserDao;
import com.freeman.domain.User;
import com.freeman.util.HibernateUtil;

public class LoginServlet extends HttpServlet {
    private UserDao userDao;

    public LoginServlet() {
        userDao = new UserDao(HibernateUtil.getSessionFactory());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String) request.getParameter("username");
        String password = (String) request.getParameter("password");

        User user = userDao.getUserByName(username);
        request.getSession().setAttribute("userId", user.getId());

        if (user != null && user.getPassword().equals(password)) {
            response.sendRedirect("./post");
        }
    }

}

# README #

 This file is a guidline to set project up and running. Follow steps below to start.

### What is this project for? ###

* The application is designed for educational purposes. It is web appliaction of a typical blog. Wlaking through this tutorial you'll get familiar with such enterprise technologies as: Hibernate, and Java Servlets API. Application uses PostgreSQL database and communicates with it via Hibernate ORM. It is not strictly layered though consists of domain and data access layer. Test Driven Development is preserved during all development process. Unit testing framework is JUnit. To build project, run unit tests and manage project is used Maven. Ready to deploy file represents itself web archive(blog.war). This file is deployed into the Tomcat servlet container.
 The main goal of this tutorial is to give basic overview of the entire web application designing from backend to the client side. So, let's get started.

### Setting up development environment ###

* Summary of set up

 All configuration consists of three main parts:
 1. set up database
 2. pull source code
 3. build and deploy

* Configuration
* Dependencies
* Database configuration

 To set up database it is necessary to install RDBMS server. For the purposes of this tutorial PostgreSQL database server is used. If it's not installed on your system download latest version from oficcial [site](http://www.enterprisedb.com/products-services-training/pgdownload) for your operation system. Except server itself it includes pgAdmin graphical tool to help you manage database. Install it with all defaults(remember user/password credentials you'll provide during installation). Once done, launch pgAdmin and try to connect to server providing credentials you specified when was installing software. There should be one default database called "postgres". Here we need to create two more databases. One to run unit tests and second to be used by application itself. Right click on "databases" in left-side menu panel and create database called "test" and "blog" respectively. That's enough for now. We'll back to database later after fetch source code.
 Second thing to do is to get the blog sources. To clone it into your local repository you need to have Git installed. Git is distributed version control system. More about it [here](http://git-scm.com/book/en/v2). Installation procedure is fairly trivial and should not take much time. Download suitable installer [here](http://git-scm.com/downloads) and install it. To check everything is fine and "git" is recognized by your operation system open command prompt(Start -> type 'cmd' in search programs field) and issue the command "git". If you see the output like this:


```
#!java

C:\Users\dnefedchenko>git
usage: git [--version] [--help] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

The most commonly used git commands are:
   add        Add file contents to the index
   bisect     Find by binary search the change that introduced a bug
   branch     List, create, or delete branches
   checkout   Checkout a branch or paths to the working tree
   clone      Clone a repository into a new directory
   commit     Record changes to the repository
   diff       Show changes between commits, commit and working tree, etc
   fetch      Download objects and refs from another repository
   grep       Print lines matching a pattern
   init       Create an empty Git repository or reinitialize an existing one
   log        Show commit logs
   merge      Join two or more development histories together
   mv         Move or rename a file, a directory, or a symlink
   pull       Fetch from and merge with another repository or a local branch
   push       Update remote refs along with associated objects
   rebase     Forward-port local commits to the updated upstream head
   reset      Reset current HEAD to the specified state
   rm         Remove files from the working tree and from the index
   show       Show various types of objects
   status     Show the working tree status
   tag        Create, list, delete or verify a tag object signed with GPG

'git help -a' and 'git help -g' lists available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.

C:\Users\dnefedchenko>
```
then your installation was successful. Let's try to pull some code. Choose place in your local file system suitable for cloning source files(it may be arbitrary folder) and type command:
```
#!java

C:\Users\dnefedchenko> git clone https://freeman_ua@bitbucket.org/freeman_ua/relational-blog.git
```
If it's been smooth new folder called relational-blog should appear in current directory. It contains all source code for blog application. Next step is to import it into Eclipse(integrated development environment). If you don't have it go to official [site](http://www.eclipse.org/downloads/?) an upload distro that fits your operation system. Extract the archive and launch IDE. Click File -> Import -> Existing Maven Projects click "Browse" button and point location of source code we've just cloned. Click "Finish". At this point you should see RelationalBlog project has been imported into workspace panel to the left. That's all for second step. Now our database server is up and running and source code is in place. Let's proceed to the last step - build and deployment.

Before our application will be deployed we need to build it to get some artifacts. Namely web archive file(blog.war). As a build tool Maven is used. More about it [here](http://maven.apache.org/download.cgi). You also need to download proper distribution for your operation system. Binary zip archive may be enough. Extract downloaded archive somewhere in your local file system(also arbitrary place). Next action is optional but desired to be done. It'll save you much time and less typing in future. Set up local environment variable to make your shell and system recognize "mvn" command from any folder within your file system. Right click on "My Computer" -> Properties -> Advanced System Settings -> Environment Variables find variable called "Path" under "System variables" section and edit it. It most likely already contains many different paths and you need to add one more in place. Just put mouse cursor at the end of this line, type ";" and paste here full path to /bin folder of the extracted Maven archive(for example D:\dnefedchenko\opt\apache-maven-3.0.4\bin). Save results and restart command prompt. Type "mvn" and observe the output:


```
#!java

C:\Users\dnefedchenko>mvn -version
Apache Maven 3.0.4 (r1232337; 2012-01-17 10:44:56+0200)
Maven home: D:\dnefedchenko\opt\apache-maven-3.0.4\bin\..
Java version: 1.7.0_25, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.7.0_25\jre
Default locale: en_US, platform encoding: Cp1252
OS name: "windows 7", version: "6.1", arch: "amd64", family: "windows"
C:\Users\dnefedchenko>
```

 If your's looks similar you're done. You can use "mvn" utility to build our source code and package it into the archive. Navigate to relational-blog folder in your command prompt.(If you're comfortable with plain shell you may use command "cd" to browse folders. If you prefer some kind of file manager like "Total Commander" or "Far" that's also fine). Make sure you are at the root of relational-blog folder(on the same level where pom.xml file resides). You're ready to start build now but first go back to eclipse and adjust configuration file to fit your personal settings. Open "test-hibernate.cfg.xml" file and find these lines: 

```
#!java

<property name="hibernate.connection.username">freeman</property>
<property name="hibernate.connection.password">qwest123</property>
<property name="hibernate.connection.url">jdbc:postgresql://localhost:5432/test</property>
```
Here you need to provide username/password credentials for your database server. Also make sure database called "test" is created. 
Issue the command:
```
#!java

D:\dnefedchenko\relational-blog> mvn clean package
```
 This command should launch build process, compile source files, run unit tests against "test" database and package everything into blog.war archive ready to be deployed. If you see "BUILD SUCCESS" output then our web archive is ready to be deployed into Apache Tomcat container. Let's download it [here](http://tomcat.apache.org/download-70.cgi). 64-bit Windows zip (pgp, md5) or 32-bit depending on your operation system should be fine. Installation procedure is the same - just extract archive. Navigate to /bin folder and try to start container:


```
#!java
D:\dnefedchenko\opt\apache-tomcat-7.0.41\bin>startup.bat

```
 It should launch servlet container and to check it open your browser and follow the url http://localhost:8080/ If you see welcome page and title kinda "If you're seeing this, you've successfully installed Tomcat. Congratulations!" it is up and running. Time to deploy our build artifacts.
 During build process Maven created folder called "target" and put our blog.war archive into it. All you need to do at the moment is just to grab this archive and put into "webapps" folder of Apache Tomcat installation(when you extracted tomcat archive you may have noticed couple of folders: /bin, /conf, /logs, /webapps etc) webapps is the one intended for deployment purposes. Cheers, blog is build and deployed! 
 Restart container:
```
#!java
D:\dnefedchenko\opt\apache-tomcat-7.0.41\bin>shutdown.bat
D:\dnefedchenko\opt\apache-tomcat-7.0.41\bin>startup.bat

```
 If no errors or failures in tomcat startup console observed go back to browser and enter url:

```
#!java
http://localhost:8080/blog/

```
 You should see Sing In page now. If it is so, congratulations, you manage to put it all together!
 Last thing is to upload some data in our database to be able to login in application. There is a file-blog.sql in the root of relational-blog folder. This is database dump needs to be restored. Go back to command line shell and give a command:

```
#!java

D:\dnefedchenko\relational-blog> psql -U username -d blog < ./blog.sql
```
Replace "username" with your valid value. You're also prompted to enter your password. If command succeeded you should see series for "CREATE" and "ALTER" statements in console. Go back to pgAdmin and check "blog" database. Now there should be four tables: users, posts, tags and comments. The one we're interested most of all right now is "users" table. Right click on it and choose "SELECT" option. It should perform select and get all records. Make sure there is a user with name "freeman" and passowrd "qwest123". These are credentials to use on Sign In page. Check this out :-)